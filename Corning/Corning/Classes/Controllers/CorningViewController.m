//
//  ConringViewController.m
//  Corning
//
//  Created by macmini_01 on 15/04/16.
//  Copyright © 2016 CNC. All rights reserved.
//

#import "CorningViewController.h"
#import "SingleIOTableViewCell.h"
#import "DoubleIOTableViewCell.h"
#import "NSString+Currency.h"
#import "CorningHelper.h"

static NSString *CellIdentifier_Single = @"Single";
static NSString *CellIdentifier_Double = @"Double";

static int TAG_ANNUAL_SPEND = 100;
static int TAG_CURRENT_DAYS = 101;
static int TAG_BEFORE_RESULT = 102;
static int TAG_FUTURE_DAYS = 103;
static int TAG_AFTER_RESULT = 104;
static int TAG_NET_CASH_FLOW = 105;
static int TAG_CURRENT_DISCOUNT = 106;
static int TAG_SUBTOTAL_CURRENT = 107;
static int TAG_CURRENT_DISCOUNT_IMPACT = 108;
static int TAG_FUTURE_DISCOUNT = 109;
static int TAG_SUBTOTAL_FUTURE = 110;
static int TAG_FUTURE_DISCOUNT_IMPACT = 111;

@interface CorningViewController ()
//payment strings
@property (nonatomic, strong) NSString *string_annualSpendWithSupplier_payment;
@property (nonatomic, strong) NSString *string_numberOfdaysCurrent_payment;
@property (nonatomic, strong) NSString *string_numberOfdaysFuture_payment;
//discount strings
@property (nonatomic, strong) NSString *string_annualSpendWithSupplier_discount;
@property (nonatomic, strong) NSString *string_numberOfdaysCurrent_discount;
@property (nonatomic, strong) NSString *string_numberOfdaysFuture_discount;
@property (nonatomic, strong) NSString *string_discountPercentageCurrent_discount;
@property (nonatomic, strong) NSString *string_discountPercentageFuture_discount;

@end

@implementation CorningViewController{
    BOOL discountMode;
    UIToolbar *keyboardDoneButtonView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStyleDone target:self
                                                                  action:@selector(doneClicked:)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    
    self.string_annualSpendWithSupplier_payment = @"1000000";
    self.string_numberOfdaysCurrent_payment = @"30";
    self.string_numberOfdaysFuture_payment = @"75";
    //discount strings
    self.string_annualSpendWithSupplier_discount = @"1000000";
    self.string_numberOfdaysCurrent_discount = @"75";
    self.string_numberOfdaysFuture_discount = @"10";
    self.string_discountPercentageCurrent_discount = @"0";
    self.string_discountPercentageFuture_discount = @"2";
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg"]];
    discountMode = NO;
    //[self setTitle:discountMode ? @"Discount Term" : @"Payment Term"];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([SingleIOTableViewCell class]) bundle:nil] forCellReuseIdentifier:CellIdentifier_Single];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([DoubleIOTableViewCell class]) bundle:nil] forCellReuseIdentifier:CellIdentifier_Double];
    [_viewInfo setFrame:self.view.bounds];
    [self.view addSubview:_viewInfo];
    [self setLeftBarButton];
    [self setRightBarButton];
    [self.tableView setTableHeaderView:[[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)]];
}


- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self performSelector:@selector(updateValues) withObject:nil afterDelay:0.2];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) setRightBarButton{
    UIButton *rightBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [rightBtn setTitle:@"?" forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor appBlueColor] forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor appDisableBlueColor] forState:UIControlStateHighlighted];
    rightBtn.layer.borderWidth = 1.0f;
    rightBtn.layer.cornerRadius = 12.5f;
    rightBtn.layer.borderColor = [[UIColor appBlueColor] CGColor];
    [rightBtn addTarget:self action:@selector(infoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:rightBtn]];
}

- (void) setLeftBarButton{
    [self setTitle:discountMode ? @"Discount Term" : @"Payment Term"];
    UIButton *leftBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 65, 30)];
    [leftBtn setTitle:discountMode ? @"Payment" : @"Discount" forState:UIControlStateNormal];
    [leftBtn setTitleColor:[UIColor appBlueColor] forState:UIControlStateNormal];
    [leftBtn setTitleColor:[UIColor appDisableBlueColor] forState:UIControlStateHighlighted];
    leftBtn.layer.borderWidth = 1.0f;
    leftBtn.layer.cornerRadius = 5.0f;
    leftBtn.titleLabel.font = [UIFont fontWithName:FONT_NAME size:13];
    leftBtn.layer.borderColor = [[UIColor appBlueColor] CGColor];
    [leftBtn addTarget:self action:@selector(discountButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:leftBtn]];
}


#pragma mark - IBAction

- (void)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (IBAction) discountButtonClicked:(id)sender{
    [self.view endEditing:YES];
    discountMode = !discountMode;
    //[self setTitle:discountMode ? @"Discount Term" : @"Payment Term"];
    [self setLeftBarButton];
    [self.tableView reloadData];
    [self performSelector:@selector(updateValues) withObject:nil afterDelay:0.2];
}

- (IBAction) infoButtonClicked:(id)sender{
    BOOL infoViewHidden = [_viewInfo isHidden];
    infoViewHidden = !infoViewHidden;
    [_viewInfo setHidden:infoViewHidden];
    if(infoViewHidden){
        [_viewInfo setAlpha:0.0];
    }else{
        [_viewInfo showPopupAnimations];
        //        NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        //        UIFont *customFont = [UIFont fontWithName:FONT_NAME size:14.0f];
        //        NSDictionary * fontAttributes = [[NSDictionary alloc] initWithObjectsAndKeys:customFont, NSFontAttributeName, nil];
        //        NSMutableAttributedString *libTitle = [[NSMutableAttributedString alloc] initWithAttributedString:attrStr];
        //         NSRange rangeOfTitle = NSMakeRange(0,[libTitle length]);
        //        [libTitle setAttributes:fontAttributes range:rangeOfTitle];
        CGFloat ratio = [[UIScreen mainScreen] bounds].size.height/480.0f;
        static CGFloat instruction_font = 17.0;
        CGFloat height = 14.0*ratio > instruction_font ? instruction_font : 14*ratio;
        NSString *htmlString = [NSString stringWithFormat:INFO_STRING,FONT_NAME,[NSNumber numberWithFloat:height]];
        [_infoinstruction loadHTMLString:htmlString baseURL:nil];
    }
}

- (IBAction)endTableEditing:(id)sender{
    [self.view endEditing:YES];
}

- (IBAction)infoOkButton:(id)sender{
    [_viewInfo setHidden:YES];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return discountMode ? 8 : 6;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //    NSInteger numberOfRows = discountMode ? 8 : 6;
    //    float ratio = [[UIScreen mainScreen] bounds].size.height / 480.0f;
    //    if (discountMode) {
    //        if(indexPath.row == numberOfRows-1){
    //            return 66*ratio;
    //        }else{
    //            return 50*ratio;
    //        }
    //    }else{
    //        if(indexPath.row == numberOfRows-1){
    //            return 86*ratio;
    //        }else{
    //            return 65*ratio;
    //        }
    //    }
    NSInteger numberOfRows = discountMode ? 8 : 6;
    float tableHeight = self.tableView.frame.size.height - 20;
    if(indexPath.row == numberOfRows-1){
        return ((tableHeight/numberOfRows)*1.25f);
    }else{
        return (tableHeight - ((tableHeight/numberOfRows)*1.25f))/(numberOfRows-1);
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    return singleTextcell;
    UITableViewCell *cell = nil;
    if(discountMode){
        switch (indexPath.row) {
            case 0:{
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier_Single];
                SingleIOTableViewCell *singleCell = (SingleIOTableViewCell *)cell;
                [singleCell enableTextField:YES];
                [[singleCell labelFirstField] setText:@"Annual Spend With Supplier"];
                singleCell.textFieldFirst.text = [NSString currencyValue:[self.string_annualSpendWithSupplier_discount doubleValue]];
                singleCell.textFieldFirst.tag = TAG_ANNUAL_SPEND;
                singleCell.textFieldFirst.delegate = self;
                
                singleCell.textFieldFirst.inputAccessoryView = keyboardDoneButtonView;
                [singleCell.textFieldFirst setKeyboardType:UIKeyboardTypeNumberPad];
            }
                break;
            case 1:{
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier_Double];
                DoubleIOTableViewCell *doubleCell = (DoubleIOTableViewCell *)cell;
                [doubleCell enableTextField:YES];
                [[doubleCell labelFirstField] setText:@"No. Of Days - Current"];
                [[doubleCell labelSecondField] setText:@"Disc. % - Current"];
                doubleCell.textFieldFirst.text = [[[NSString currencyValue:[self.string_numberOfdaysCurrent_discount doubleValue]] stringByReplacingOccurrencesOfString:@"$" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
                doubleCell.textFieldSecond.text = [NSString percentageValue:[self.string_discountPercentageCurrent_discount doubleValue]];
                doubleCell.textFieldFirst.tag=TAG_CURRENT_DAYS;
                doubleCell.textFieldSecond.tag=TAG_CURRENT_DISCOUNT;
                doubleCell.textFieldFirst.delegate = self;
                doubleCell.textFieldSecond.delegate = self;
                
                doubleCell.textFieldFirst.inputAccessoryView = keyboardDoneButtonView;
                doubleCell.textFieldSecond.inputAccessoryView = keyboardDoneButtonView;
                [doubleCell.textFieldFirst setKeyboardType:UIKeyboardTypeNumberPad];
                [doubleCell.textFieldSecond setKeyboardType:UIKeyboardTypeDecimalPad];
            }
                break;
            case 2:{
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier_Double];
                DoubleIOTableViewCell *doubleCell = (DoubleIOTableViewCell *)cell;
                [doubleCell enableTextField:NO];
                [[doubleCell labelFirstField] setText:@"Subtotal - Current"];
                [[doubleCell labelSecondField] setText:@"Disc. Impact - Current"];
                doubleCell.textFieldFirst.tag=TAG_SUBTOTAL_CURRENT;
                doubleCell.textFieldSecond.tag=TAG_CURRENT_DISCOUNT_IMPACT;
            }
                break;
            case 3:{
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier_Single];
                [(SingleIOTableViewCell *)cell enableTextField:NO];
                SingleIOTableViewCell *singleCell = (SingleIOTableViewCell *)cell;
                [[singleCell labelFirstField] setText:@"Before Net Impact"];
                singleCell.textFieldFirst.tag=TAG_BEFORE_RESULT;
            }
                break;
            case 4:{
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier_Double];
                [(DoubleIOTableViewCell *)cell enableTextField:YES];
                DoubleIOTableViewCell *doubleCell = (DoubleIOTableViewCell *)cell;
                doubleCell.labelFirstField.text = @"No. Of Days - Future";
                doubleCell.labelSecondField.text = @"Disc. % - Future";
                doubleCell.textFieldFirst.text = [[[NSString currencyValue:[self.string_numberOfdaysFuture_discount doubleValue]] stringByReplacingOccurrencesOfString:@"$" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];;
                doubleCell.textFieldSecond.text = [NSString percentageValue:[self.string_discountPercentageFuture_discount doubleValue]];
                doubleCell.textFieldFirst.tag=TAG_FUTURE_DAYS;
                doubleCell.textFieldSecond.tag=TAG_FUTURE_DISCOUNT;
                doubleCell.textFieldFirst.delegate = self;
                doubleCell.textFieldSecond.delegate = self;
                doubleCell.textFieldFirst.inputAccessoryView = keyboardDoneButtonView;
                doubleCell.textFieldSecond.inputAccessoryView = keyboardDoneButtonView;
                [doubleCell.textFieldFirst setKeyboardType:UIKeyboardTypeNumberPad];
                [doubleCell.textFieldSecond setKeyboardType:UIKeyboardTypeDecimalPad];
            }
                break;
            case 5:{
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier_Double];
                [(DoubleIOTableViewCell *)cell enableTextField:NO];
                DoubleIOTableViewCell *doubleCell = (DoubleIOTableViewCell *)cell;
                doubleCell.labelFirstField.text = @"Subtotal - Future";
                doubleCell.labelSecondField.text = @"Disc. Impact - Future";
                doubleCell.textFieldFirst.tag=TAG_SUBTOTAL_FUTURE;
                doubleCell.textFieldSecond.tag=TAG_FUTURE_DISCOUNT_IMPACT;
            }
                break;
            case 6:{
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier_Single];
                [(SingleIOTableViewCell *)cell enableTextField:NO];
                SingleIOTableViewCell *singleCell = (SingleIOTableViewCell *)cell;
                [[singleCell labelFirstField] setText:@"After Net Impact"];
                singleCell.textFieldFirst.tag=TAG_AFTER_RESULT;
            }
                break;
            case 7:{
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier_Single];
                SingleIOTableViewCell *singleCell = (SingleIOTableViewCell *)cell;
                [singleCell enableTextField:NO];
                [[singleCell labelFirstField] setText:@"NET CASH FLOW IMPACT"];
                singleCell.textFieldFirst.tag=TAG_NET_CASH_FLOW;
            }
                break;
        }
    }else{
        switch (indexPath.row) {
            case 0:{
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier_Single];
                [(SingleIOTableViewCell *)cell enableTextField:YES];
                SingleIOTableViewCell *singleCell = (SingleIOTableViewCell *)cell;
                singleCell.labelFirstField.text = @"Annual Spend With Supplier";
                singleCell.textFieldFirst.text = [NSString currencyValue:[self.string_annualSpendWithSupplier_payment doubleValue]];
                singleCell.textFieldFirst.tag = TAG_ANNUAL_SPEND;
                [singleCell.textFieldFirst setDelegate:self];
                singleCell.textFieldFirst.inputAccessoryView = keyboardDoneButtonView;
                [singleCell.textFieldFirst setKeyboardType:UIKeyboardTypeNumberPad];
            }
                break;
            case 1:{
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier_Single];
                [(SingleIOTableViewCell *)cell enableTextField:YES];
                SingleIOTableViewCell *singleCell = (SingleIOTableViewCell *)cell;
                singleCell.labelFirstField.text = @"No. Of Days - Current";
                singleCell.textFieldFirst.text = [[[NSString currencyValue:[self.string_numberOfdaysCurrent_payment doubleValue]] stringByReplacingOccurrencesOfString:@"$" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
                singleCell.textFieldFirst.tag=TAG_CURRENT_DAYS;
                [singleCell.textFieldFirst setDelegate:self];
                singleCell.textFieldFirst.inputAccessoryView = keyboardDoneButtonView;
                [singleCell.textFieldFirst setKeyboardType:UIKeyboardTypeNumberPad];
            }
                break;
            case 2:{
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier_Single];
                [(SingleIOTableViewCell *)cell enableTextField:NO];
                SingleIOTableViewCell *singleCell = (SingleIOTableViewCell *)cell;
                singleCell.labelFirstField.text = @"Before Results";
                singleCell.textFieldFirst.tag=TAG_BEFORE_RESULT;
            }
                break;
            case 3:{
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier_Single];
                [(SingleIOTableViewCell *)cell enableTextField:YES];
                SingleIOTableViewCell *singleCell = (SingleIOTableViewCell *)cell;
                singleCell.labelFirstField.text = @"No. Of Days - Future";
                singleCell.textFieldFirst.text = [[[NSString currencyValue:[self.string_numberOfdaysFuture_payment doubleValue]] stringByReplacingOccurrencesOfString:@"$" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
                singleCell.textFieldFirst.tag=TAG_FUTURE_DAYS;
                [singleCell.textFieldFirst setDelegate:self];
                singleCell.textFieldFirst.inputAccessoryView = keyboardDoneButtonView;
                [singleCell.textFieldFirst setKeyboardType:UIKeyboardTypeNumberPad];
                
            }
                break;
            case 4:{
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier_Single];
                [(SingleIOTableViewCell *)cell enableTextField:NO];
                SingleIOTableViewCell *singleCell = (SingleIOTableViewCell *)cell;
                singleCell.labelFirstField.text = @"After Results";
                singleCell.textFieldFirst.tag=TAG_AFTER_RESULT;
            }
                break;
            case 5:{
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier_Single];
                [(SingleIOTableViewCell *)cell enableTextField:NO];
                SingleIOTableViewCell *singleCell = (SingleIOTableViewCell *)cell;
                singleCell.labelFirstField.text = @"NET CASH FLOW IMPACT";
                singleCell.textFieldFirst.tag=TAG_NET_CASH_FLOW;
            }
                break;
        }
    }
    
    static CGFloat max_font = 17.0f;
    static CGFloat min_font = 13.0f;
    CGFloat ratio = [[UIScreen mainScreen] bounds].size.width/320.0f;
    CGFloat newFontSize = min_font*ratio > max_font ? max_font : min_font*ratio;
    UIFont *labelFont = [UIFont fontWithName:FONT_NAME size:newFontSize];
    
    if(discountMode){
        if(indexPath.row == 7){
            SingleIOTableViewCell *singleCell = (SingleIOTableViewCell *)cell;
            [singleCell.labelFirstField setFont:[UIFont fontWithName:FONT_NAME size:18]];
            [singleCell.textFieldFirst setFont:[UIFont fontWithName:FONT_NAME size:32]];
        }else if([cell isKindOfClass:[SingleIOTableViewCell class]]){
            SingleIOTableViewCell *singleCell = (SingleIOTableViewCell *)cell;
            [singleCell.labelFirstField setFont:labelFont];
            [singleCell.textFieldFirst setFont:[UIFont fontWithName:FONT_NAME size:18]];
        }else if([cell isKindOfClass:[DoubleIOTableViewCell class]]){
            DoubleIOTableViewCell *doubleCell = (DoubleIOTableViewCell *)cell;
            [doubleCell.labelFirstField setFont:labelFont];
            [doubleCell.textFieldFirst setFont:[UIFont fontWithName:FONT_NAME size:18]];
            [doubleCell.labelSecondField setFont:labelFont];
            [doubleCell.textFieldSecond setFont:[UIFont fontWithName:FONT_NAME size:18]];
        }
    }else{
        if(indexPath.row == 5){
            SingleIOTableViewCell *singleCell = (SingleIOTableViewCell *)cell;
            [singleCell.labelFirstField setFont:[UIFont fontWithName:FONT_NAME size:18]];
            [singleCell.textFieldFirst setFont:[UIFont fontWithName:FONT_NAME size:32]];
        }else if([cell isKindOfClass:[SingleIOTableViewCell class]]){
            SingleIOTableViewCell *singleCell = (SingleIOTableViewCell *)cell;
            [singleCell.labelFirstField setFont:labelFont];
            [singleCell.textFieldFirst setFont:[UIFont fontWithName:FONT_NAME size:18]];
        }
    }
    
    [cell isKindOfClass:[SingleIOTableViewCell class]] ? [(SingleIOTableViewCell *)cell setTextfieldDelegate:self] : [(DoubleIOTableViewCell *)cell setTextfieldDelegate:self];
    return cell;
}


#pragma mark - TextField Delegates

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField.tag == TAG_ANNUAL_SPEND
        || textField.tag == TAG_FUTURE_DISCOUNT
        || textField.tag == TAG_CURRENT_DISCOUNT
        || textField.tag == TAG_CURRENT_DAYS
        || textField.tag == TAG_FUTURE_DAYS) {
        NSString *newStr = [textField.text plainStringValue];
        textField.text = newStr;
    }
}

- (void) textFieldDidEndEditing:(UITextField *)textField{
    if (textField.tag == TAG_ANNUAL_SPEND){
        if(discountMode){
            self.string_annualSpendWithSupplier_discount = textField.text;
        }else{
            self.string_annualSpendWithSupplier_payment = textField.text;
        }
        textField.text = [NSString currencyValue:[[textField.text plainStringValue] doubleValue]];
    }else if(textField.tag == TAG_FUTURE_DISCOUNT || textField.tag == TAG_CURRENT_DISCOUNT){
        if(textField.tag == TAG_FUTURE_DISCOUNT){
            self.string_discountPercentageFuture_discount = textField.text;
        }else{
            self.string_discountPercentageCurrent_discount = textField.text;
        }
        textField.text = [NSString percentageValue:[[textField.text plainStringValue] doubleValue]];
    }else if (textField.tag == TAG_CURRENT_DAYS || textField.tag == TAG_FUTURE_DAYS){
        if(discountMode){
            if(textField.tag == TAG_FUTURE_DAYS){
                self.string_numberOfdaysFuture_discount = textField.text;
            }else{
                self.string_numberOfdaysCurrent_discount = textField.text;
            }
        }else{
            if(textField.tag == TAG_FUTURE_DAYS){
                self.string_numberOfdaysFuture_payment = textField.text;
            }else{
                self.string_numberOfdaysCurrent_payment = textField.text;
            }
        }
        
        NSString *tempString = [NSString currencyValue:[[textField.text plainStringValue] doubleValue]];
        textField.text = [tempString stringByReplacingOccurrencesOfString:@"$" withString:@""];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    [self performSelector:@selector(updateValues) withObject:nil afterDelay:0.2];
    return YES;
}

- (void) updateValues{
    if(discountMode){
//        self.string_annualSpendWithSupplier_discount = [(UITextField *)[self.tableView viewWithTag:TAG_ANNUAL_SPEND] text];
//        self.string_numberOfdaysCurrent_discount =[(UITextField *)[self.tableView viewWithTag:TAG_CURRENT_DAYS] text];
//        self.string_numberOfdaysFuture_discount = [(UITextField *)[self.tableView viewWithTag:TAG_FUTURE_DAYS] text];
//        self.string_discountPercentageCurrent_discount = [(UITextField *)[self.tableView viewWithTag:TAG_CURRENT_DISCOUNT] text];
//        self.string_discountPercentageFuture_discount = [(UITextField *)[self.tableView viewWithTag:TAG_FUTURE_DISCOUNT] text];
        //subtotal-current
        [(UITextField *)[self.tableView viewWithTag:TAG_SUBTOTAL_CURRENT] setText:[CorningHelper getResultForAnnualSpend:[(UITextField *)[self.tableView viewWithTag:TAG_ANNUAL_SPEND] text] andDays:[(UITextField *)[self.tableView viewWithTag:TAG_CURRENT_DAYS] text]]];
        //subtotal-future
        [(UITextField *)[self.tableView viewWithTag:TAG_SUBTOTAL_FUTURE] setText:[CorningHelper getResultForAnnualSpend:[(UITextField *)[self.tableView viewWithTag:TAG_ANNUAL_SPEND] text] andDays:[(UITextField *)[self.tableView viewWithTag:TAG_FUTURE_DAYS] text]]];
        //discount impact-current
        [(UITextField *)[self.tableView viewWithTag:TAG_CURRENT_DISCOUNT_IMPACT] setText:[CorningHelper getDiscountForAnnualSpend:[(UITextField *)[self.tableView viewWithTag:TAG_ANNUAL_SPEND] text] andDiscount:[(UITextField *)[self.tableView viewWithTag:TAG_CURRENT_DISCOUNT] text]]];
        //discount impact-future
        [(UITextField *)[self.tableView viewWithTag:TAG_FUTURE_DISCOUNT_IMPACT] setText:[CorningHelper getDiscountForAnnualSpend:[(UITextField *)[self.tableView viewWithTag:TAG_ANNUAL_SPEND] text] andDiscount:[(UITextField *)[self.tableView viewWithTag:TAG_FUTURE_DISCOUNT] text]]];
        //before-net-impact
        [(UITextField *)[self.tableView viewWithTag:TAG_BEFORE_RESULT] setText:[CorningHelper getNetImpactForSubtotal:[(UITextField *)[self.tableView viewWithTag:TAG_SUBTOTAL_CURRENT]text] andDiscount:[(UITextField *)[self.tableView viewWithTag:TAG_CURRENT_DISCOUNT_IMPACT]text]]];
        //after-net-impact
        [(UITextField *)[self.tableView viewWithTag:TAG_AFTER_RESULT] setText:[CorningHelper getNetImpactForSubtotal:[(UITextField *)[self.tableView viewWithTag:TAG_SUBTOTAL_FUTURE]text] andDiscount:[(UITextField *)[self.tableView viewWithTag:TAG_FUTURE_DISCOUNT_IMPACT]text]]];
        //Net Cash Flow
        [(UITextField *)[self.tableView viewWithTag:TAG_NET_CASH_FLOW] setText:[CorningHelper getNetCashFlowImpactForBeforeResult:[(UITextField *)[self.tableView viewWithTag:TAG_BEFORE_RESULT]text] andAfterResult:[(UITextField *)[self.tableView viewWithTag:TAG_AFTER_RESULT]text]]];
    }else{
//        self.string_annualSpendWithSupplier_payment = [(UITextField *)[self.tableView viewWithTag:TAG_ANNUAL_SPEND] text];
//        self.string_numberOfdaysCurrent_payment = [(UITextField *)[self.tableView viewWithTag:TAG_CURRENT_DAYS] text];
//        self.string_numberOfdaysFuture_payment = [(UITextField *)[self.tableView viewWithTag:TAG_FUTURE_DAYS] text];
        //Before Result
        [(UITextField *)[self.tableView viewWithTag:TAG_BEFORE_RESULT] setText:[CorningHelper getResultForAnnualSpend:[(UITextField *)[self.tableView viewWithTag:TAG_ANNUAL_SPEND] text] andDays:[(UITextField *)[self.tableView viewWithTag:TAG_CURRENT_DAYS] text]]];
        //After Result
        [(UITextField *)[self.tableView viewWithTag:TAG_AFTER_RESULT] setText:[CorningHelper getResultForAnnualSpend:[(UITextField *)[self.tableView viewWithTag:TAG_ANNUAL_SPEND] text] andDays:[(UITextField *)[self.tableView viewWithTag:TAG_FUTURE_DAYS] text]]];
        //Net Cash Flow
        [(UITextField *)[self.tableView viewWithTag:TAG_NET_CASH_FLOW] setText:[CorningHelper getNetCashFlowImpactForBeforeResult:[(UITextField *)[self.tableView viewWithTag:TAG_BEFORE_RESULT]text] andAfterResult:[(UITextField *)[self.tableView viewWithTag:TAG_AFTER_RESULT]text]]];
    }
    [self setColortext];
}

-(void)setColortext{
    NSString *tempString = [(UITextField *)[self.tableView viewWithTag:TAG_NET_CASH_FLOW] text];
    double totalCashImpact = [[tempString plainStringValue] floatValue];
    //    NSLog(@"%f",totalCashImpact);
    UITextField *textfield = [self.tableView viewWithTag:TAG_NET_CASH_FLOW];
    if(totalCashImpact < 0){
        UIColor *color = [UIColor colorWithRed:255/255 green:51/255 blue:0/255 alpha:1];
        [self setAtrributedText:textfield foregroundColor:color borderColor:[UIColor whiteColor]];
    }else{
        UIColor *color = [UIColor colorWithRed:0/255 green:255/255 blue:51/255 alpha:1];
        [self setAtrributedText:textfield foregroundColor:color borderColor:[UIColor whiteColor]];
    }
}

-(void)setAtrributedText:(UITextField *) textfield foregroundColor:(UIColor *)color borderColor:(UIColor *) borderColor{
    NSDictionary* strokeTextAttributes = @{
                                           NSStrokeColorAttributeName : borderColor,
                                           NSStrokeWidthAttributeName : [NSNumber numberWithFloat:0.0],
                                           NSForegroundColorAttributeName : color,
                                           NSFontAttributeName : [UIFont fontWithName:FONT_NAME size:18]
                                           };
    textfield.attributedText = [[NSAttributedString alloc] initWithString:[textfield text] attributes:strokeTextAttributes];
}
@end
