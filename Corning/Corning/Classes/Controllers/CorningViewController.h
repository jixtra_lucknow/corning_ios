//
//  ConringViewController.h
//  Corning
//
//  Created by macmini_01 on 15/04/16.
//  Copyright © 2016 CNC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CorningViewController : UITableViewController<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) IBOutlet UIView *viewInfo;
@property (nonatomic, strong) IBOutlet UIWebView *infoinstruction;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *barBtnLeft;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *barBtnRight;
@end
