//
//  CorningHelper.h
//  Corning
//
//  Created by macmini_01 on 20/04/16.
//  Copyright © 2016 CNC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CorningHelper : NSObject

+ (NSString *) getResultForAnnualSpend:(NSString *) annualSpend andDays: (NSString *) days;
+ (NSString *) getNetCashFlowImpactForBeforeResult:(NSString *) beforeResult andAfterResult:(NSString *) afterResult;
+ (NSString *) getDiscountForAnnualSpend:(NSString *) annualSpend andDiscount:(NSString *) discount;
+ (NSString *) getNetImpactForSubtotal:(NSString *) subtotal andDiscount:(NSString *) discount;
@end
