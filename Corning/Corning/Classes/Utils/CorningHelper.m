//
//  CorningHelper.m
//  Corning
//
//  Created by macmini_01 on 20/04/16.
//  Copyright © 2016 CNC. All rights reserved.
//

#import "CorningHelper.h"
#import "NSString+Currency.h"

@implementation CorningHelper

+ (NSString *) getResultForAnnualSpend:(NSString *) annualSpend andDays: (NSString *) days{
    double annualSpendValue = [[annualSpend plainStringValue] doubleValue];
    double daysValue = [[days plainStringValue] doubleValue];
    double result = (annualSpendValue*daysValue*0.1)/365;
    return [[NSString currencyValue:result] containsString:@"."] ? [NSString currencyValue:result] : [NSString stringWithFormat:@"%@.00",[NSString currencyValue:result]];
}

+ (NSString *) getNetCashFlowImpactForBeforeResult:(NSString *) beforeResult andAfterResult:(NSString *) afterResult{
    double beforeResultValue = [[beforeResult plainStringValue] doubleValue];
    double afterResultValue = [[afterResult plainStringValue] doubleValue];
    double result = afterResultValue - beforeResultValue;
    return [[NSString currencyValue:result] containsString:@"."] ? [NSString currencyValue:result] : [NSString stringWithFormat:@"%@.00",[NSString currencyValue:result]];
}

+ (NSString *) getDiscountForAnnualSpend:(NSString *) annualSpend andDiscount:(NSString *) discount{
    double annualSpendValue = [[annualSpend plainStringValue] doubleValue];
    double discountValue = [[discount plainStringValue] doubleValue];
    double result = (annualSpendValue*discountValue)/100;
    return [[NSString currencyValue:result] containsString:@"."] ? [NSString currencyValue:result] : [NSString stringWithFormat:@"%@.00",[NSString currencyValue:result]];
}

+ (NSString *) getNetImpactForSubtotal:(NSString *) subtotal andDiscount:(NSString *) discount{
    double subtotalValue = [[subtotal plainStringValue] doubleValue];
    double discountValue = [[discount plainStringValue] doubleValue];
    double result = subtotalValue + discountValue;
    return [[NSString currencyValue:result] containsString:@"."] ? [NSString currencyValue:result] : [NSString stringWithFormat:@"%@.00",[NSString currencyValue:result]];
}
@end
