//
//  AppConstants.h
//  Corning
//
//  Created by Byte Slick on 18/04/16.
//  Copyright © 2016 CNC. All rights reserved.
//
#ifndef AppConstants_h
#define AppConstants_h

static NSString *const FONT_NAME = @"TheSansPlain";


static NSString *const INFO_STRING = @"<html>\
<style>.font{\
font-family:%@;\
font-size:%@\
}\
</style>\
<body class = 'font'>\
<p>This tool is designed to:</p>\
<p>1. Evaluate changes in payment and discount term scenarios to determine the net impact to cash flow</p>\
<p>2. Compare \"Current State\" to \"Future State\"</p>\
<p>3. Develop negotiation strategies</p>\
<p>Example: What scenarios of payment terms or discount terms would be cash neutral to the Company, What Various scenarios of payment terms or discount terms would maximize the Company's cash flow.</p>\
<u>Instructions</u>\
<p>1. Enter data into the appropriate cells highlighted in yellow.</p>\
<p>2. \"Current State\" designates the current situation - payment and/or discount term.</p>\
<p>3. \"Future State\" designates the proposed changes - payment and/or discount term.</p>\
<p>4. \"Net Cash Flow Impact\" calculates the net change in  cash flow going from the \"Current State\" to the \"Future State\". A positive number indicates a positive impact to the Company's cash flow. A negative number indicates a negative impact to the Company's cash flow.</p>\
<p>5. We have chosen annual cost of capital to be 10&#37; for the calculations.</p>\
</body>\
</html>";

#endif /* AppConstants_h */
