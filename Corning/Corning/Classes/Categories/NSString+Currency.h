//
//  NSString+Currency.h
//  CNC
//
//  Created by Byte Slick on 23/11/15.
//  Copyright © 2015 AppUs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Currency)
- (NSString *) plainStringValue;
+ (NSString *) currencyValue:(double) input;
+ (NSString *) percentageValue:(double) input;
@end
