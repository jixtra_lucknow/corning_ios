//
//  UIColor+Color.m
//  Corning
//
//  Created by macmini_01 on 15/04/16.
//  Copyright © 2016 CNC. All rights reserved.
//

#import "UIColor+Color.h"

@implementation UIColor (Color)
+(UIColor *)yellowTextColor{
    return [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
}

+(UIColor *) appBlueColor{
    return [UIColor colorWithRed:0.0f/255.0f green:103.0f/255.0f blue:171.0f/255.0f alpha:1.0f];
}

+(UIColor *) appDisableBlueColor{
    return [UIColor colorWithRed:0.0f/255.0f green:103.0f/255.0f blue:171.0f/255.0f alpha:0.6f];
}

@end
