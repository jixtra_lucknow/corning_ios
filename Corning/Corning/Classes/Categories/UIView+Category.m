//
//  UIView+Category.m
//  Crue
//
//  Created by Byte Slick on 01/08/13.
//  Copyright (c) 2013 Byte Slick. All rights reserved.
//

#import "UIView+Category.h"

@implementation UIView (Popup_Animations)
- (void) showPopupAnimations{
    self.alpha = 0;
    [UIView animateWithDuration:0.3 animations:^{self.alpha = 1.0;}];
    [self viewWithTag:101].layer.cornerRadius = 11.0f;
    self.layer.transform = CATransform3DMakeScale(0.5, 0.5, 1.0);
    CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
//    bounceAnimation.values = [NSArray arrayWithObjects:
//                              [NSNumber numberWithFloat:0.5],
//                              [NSNumber numberWithFloat:1.1],
//                              [NSNumber numberWithFloat:0.8],
//                              [NSNumber numberWithFloat:1.0], nil];
    bounceAnimation.duration = 0.3;
    bounceAnimation.removedOnCompletion = NO;
    [self.layer addAnimation:bounceAnimation forKey:@"bounce"];
    self.layer.transform = CATransform3DIdentity;
}
@end
