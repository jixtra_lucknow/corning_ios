//
//  UIView+Category.h
//  Crue
//
//  Created by Byte Slick on 01/08/13.
//  Copyright (c) 2013 Byte Slick. All rights reserved.
//


#import <QuartzCore/QuartzCore.h>
@interface UIView (Popup_Animations)
- (void) showPopupAnimations;
@end
