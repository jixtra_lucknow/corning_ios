//
//  UIColor+Color.h
//  Corning
//
//  Created by macmini_01 on 15/04/16.
//  Copyright © 2016 CNC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Color)
+(UIColor *)yellowTextColor;
+(UIColor *) appBlueColor;
+(UIColor *) appDisableBlueColor;
@end
