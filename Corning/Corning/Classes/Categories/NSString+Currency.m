//
//  NSString+Currency.m
//  CNC
//
//  Created by Byte Slick on 23/11/15.
//  Copyright © 2015 AppUs. All rights reserved.
//

#import "NSString+Currency.h"

static NSNumberFormatter *currencyFormatter = nil;
static NSNumberFormatter *percentageFormatter = nil;

@implementation NSString (Currency)
- (NSString *) plainStringValue{
    return [[[[self stringByReplacingOccurrencesOfString:@"," withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"$" withString:@""] stringByReplacingOccurrencesOfString:@"%" withString:@""];
}

+ (NSString *) currencyValue:(double) input{
    if(percentageFormatter == nil){
        percentageFormatter = [[NSNumberFormatter alloc] init];
        [percentageFormatter setPositiveFormat:@"0.##"];
    }
    NSString *tempString = [percentageFormatter stringFromNumber:[NSNumber numberWithDouble:input]];
    if(currencyFormatter == nil){
        currencyFormatter = [[NSNumberFormatter alloc] init];
        [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [currencyFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    }
    return [NSString stringWithFormat:@"$ %@",[currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[tempString doubleValue]]]];
}

+ (NSString *) percentageValue:(double) input{
    if(percentageFormatter == nil){
        percentageFormatter = [[NSNumberFormatter alloc] init];
        [percentageFormatter setPositiveFormat:@"0.##"];
    }
    NSString *tempString = [percentageFormatter stringFromNumber:[NSNumber numberWithDouble:input]];
    if(currencyFormatter == nil){
        currencyFormatter = [[NSNumberFormatter alloc] init];
        [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [currencyFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    }
    NSString *result = [NSString stringWithFormat:@"%@",[currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[tempString doubleValue]]]];
    
    return [result containsString:@"."] ? [NSString stringWithFormat:@"%@ %%",result] : [NSString stringWithFormat:@"%@.00 %%",result];
}
@end
