//
//  DoubleIOTableViewCell.h
//  Corning
//
//  Created by Byte Slick on 18/04/16.
//  Copyright © 2016 CNC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoubleIOTableViewCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UILabel *labelFirstField;
@property (nonatomic, strong) IBOutlet UILabel *labelSecondField;
@property (nonatomic, strong) IBOutlet UITextField *textFieldFirst;
@property (nonatomic, strong) IBOutlet UITextField *textFieldSecond;
@property (nonatomic, strong) IBOutlet UIView *textFieldContainer;
@property (nonatomic, strong) id textfieldDelegate;

- (void) enableTextField:(BOOL) enable;
@end
