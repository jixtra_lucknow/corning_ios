//
//  SingleIOTableViewCell.m
//  Corning
//
//  Created by Byte Slick on 18/04/16.
//  Copyright © 2016 CNC. All rights reserved.
//

#import "SingleIOTableViewCell.h"

@implementation SingleIOTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [_textFieldFirst setFont:[UIFont fontWithName:FONT_NAME size:20.0]];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    [_textFieldFirst setRightView:paddingView];
    [_textFieldFirst setRightViewMode:UITextFieldViewModeAlways];
}

- (void) enableTextField:(BOOL) enable{
    [_textFieldFirst setUserInteractionEnabled:enable];
    [_textFieldFirst setBackgroundColor:enable ? [UIColor colorWithRed:255.0f/255.0f green:254.0f/255.0f blue:162.0f/255.0f alpha:1] : [UIColor clearColor]];
    [_textFieldFirst setTextColor:enable ? [UIColor blackColor] : [UIColor whiteColor]];
    [_textFieldFirst setDelegate:_textfieldDelegate];
}

//-(void)setFontandSize{
//    [_textFieldFirst setFont:[UIFont fontWithName:FONT_NAME size:22.0]];
//    [_labelFirstField setFont:[UIFont fontWithName:FONT_NAME size:15.0]];
//}

@end
