//
//  DoubleIOTableViewCell.m
//  Corning
//
//  Created by Byte Slick on 18/04/16.
//  Copyright © 2016 CNC. All rights reserved.
//

#import "DoubleIOTableViewCell.h"

@implementation DoubleIOTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    UIFont *font = [UIFont fontWithName:FONT_NAME size:20.0];
    [_textFieldFirst setFont:font];
    [_textFieldSecond setFont:font];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    UIView *paddingViewFirst = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    [_textFieldFirst setRightView:paddingViewFirst];
    [_textFieldFirst setRightViewMode:UITextFieldViewModeAlways];
    UIView *paddingViewSecond = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    [_textFieldSecond setRightView:paddingViewSecond];
    [_textFieldSecond setRightViewMode:UITextFieldViewModeAlways];
//    _textFieldFirst.layer.sublayerTransform = CATransform3DMakeTranslation(-5, 0, 0);
//    _textFieldSecond.layer.sublayerTransform = CATransform3DMakeTranslation(-5, 0, 0);
    // Initialization code
}

- (void) enableTextField:(BOOL) enable{
    [_textFieldFirst setUserInteractionEnabled:enable];
    [_textFieldFirst setBackgroundColor:enable ? [UIColor colorWithRed:255.0f/255.0f green:254.0f/255.0f blue:162.0f/255.0f alpha:1] : [UIColor clearColor]];
    [_textFieldFirst setTextColor:enable ? [UIColor blackColor] : [UIColor whiteColor]];
    [_textFieldFirst setDelegate:_textfieldDelegate];
    [_textFieldSecond setUserInteractionEnabled:enable];
    [_textFieldSecond setBackgroundColor:enable ? [UIColor colorWithRed:255.0f/255.0f green:254.0f/255.0f blue:162.0f/255.0f alpha:1] : [UIColor clearColor]];
    [_textFieldSecond setTextColor:enable ? [UIColor blackColor] : [UIColor whiteColor]];
    [_textFieldSecond setDelegate:_textfieldDelegate];
}

@end
