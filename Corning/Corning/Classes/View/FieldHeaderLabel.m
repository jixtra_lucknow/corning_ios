//
//  FieldHeaderLabel.m
//  Corning
//
//  Created by Byte Slick on 18/04/16.
//  Copyright © 2016 CNC. All rights reserved.
//

#import "FieldHeaderLabel.h"

@implementation FieldHeaderLabel

- (void)awakeFromNib {
    [super awakeFromNib];
//    CGFloat ratio = [[UIScreen mainScreen] bounds].size.height/480.0f;
    static CGFloat max_font = 10.0;
//    CGFloat height = 12.0*ratio > max_font ? max_font : 12*ratio;
    UIFont *font = [UIFont fontWithName:FONT_NAME size:max_font];
    [self setFont:font];
    [self setAdjustsFontSizeToFitWidth:YES];
//    [self setMinimumScaleFactor:0.8];
    [self setTextColor:[UIColor whiteColor]];
    // Initialization code
}
@end
