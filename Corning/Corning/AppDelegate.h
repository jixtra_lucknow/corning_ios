//
//  AppDelegate.h
//  Corning
//
//  Created by macmini_01 on 15/04/16.
//  Copyright © 2016 CNC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

